package com.serve.DicksSportingGoods.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.serve.DicksSportingGoods.Dto.DepartmentDto;
import com.serve.DicksSportingGoods.Service.DepartmentService;

@RestController
public class DepartmentController {
	
	@Autowired
	  DepartmentService departmentService;

	  @RequestMapping(value = "users", method = RequestMethod.GET,
	      produces = "application/json")
	  public @ResponseBody ResponseEntity<List<DepartmentDto>> findAll() {
	   
		  List<DepartmentDto> departmentDtoList = departmentService.findAll();
	   
	    if (departmentDtoList==null)
	      return new ResponseEntity<List<DepartmentDto>>(HttpStatus.NO_CONTENT);
	    else
	      return new ResponseEntity<List<DepartmentDto>>(departmentDtoList, HttpStatus.OK);
	  }

	
	
	@RequestMapping(value = "users/save", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> saveDepartment(@RequestBody List<DepartmentDto> departmentDto) {
		
		String result = departmentService.saveDepartments(departmentDto);
		if (null == result)
			return new ResponseEntity<String>(result, HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	 
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  @RequestMapping(value = "hello", method = RequestMethod.GET)
		  public @ResponseBody ResponseEntity <String> testingHello() {
		  
		  String s = "Hello";
		      return new ResponseEntity<String>(s, HttpStatus.OK);
		  }
}
