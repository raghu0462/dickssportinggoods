package com.serve.DicksSportingGoods.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.serve.DicksSportingGoods.Dto.CategoriesDto;
import com.serve.DicksSportingGoods.Service.CategoryService;

@RestController
public class CategoriesController {
	
	@Autowired
	CategoryService categoryservice;
	
	@RequestMapping(value = "allCategories", method = RequestMethod.GET,
	      produces = "application/json")
	  public @ResponseBody ResponseEntity<List<CategoriesDto>> findAll() {
	   
		  List<CategoriesDto> categoriesDtoList = categoryservice.findAll();
	   
	    if (categoriesDtoList==null)
	      return new ResponseEntity<List<CategoriesDto>>(HttpStatus.NO_CONTENT);
	    else
	      return new ResponseEntity<List<CategoriesDto>>(categoriesDtoList, HttpStatus.OK);
	  }

	
	@RequestMapping(value = "Categories/save", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> saveCategories(@RequestBody List<CategoriesDto> categoriesDto) {
		
		String result = categoryservice.saveCategories(categoriesDto);
		if (null == result)
			return new ResponseEntity<String>(result, HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	 
}
