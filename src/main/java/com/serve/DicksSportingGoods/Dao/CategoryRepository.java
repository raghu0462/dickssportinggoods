package com.serve.DicksSportingGoods.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.serve.DicksSportingGoods.Model.Categories;
@Repository
public interface CategoryRepository extends CrudRepository<Categories,Integer> {

}
