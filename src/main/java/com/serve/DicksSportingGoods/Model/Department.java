package com.serve.DicksSportingGoods.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "department")
public class Department implements Serializable{
	
private static final long serialVersionUID = 1L;

@Id
private int departmentId;

@Column(name = "footware")
private String footware;

@Column(name = "apparel")
private String apparel;

@Column(name = "outerware")
private String outerware;

@Column(name = "accessories")
private String accessories;

@Column(name = "fan_shop")
private String fanShop;

@Column(name = "featured")
private String featured;

@Column(name = "clearance")
private String clearance;

public int getDepartmentId() {
	return departmentId;
}

public void setDepartmentId(int departmentId) {
	this.departmentId = departmentId;
}

public String getFootware() {
	return footware;
}

public void setFootware(String footware) {
	this.footware = footware;
}

public String getApparel() {
	return apparel;
}

public void setApparel(String apparel) {
	this.apparel = apparel;
}

public String getOuterware() {
	return outerware;
}

public void setOuterware(String outerware) {
	this.outerware = outerware;
}

public String getAccessories() {
	return accessories;
}

public void setAccessories(String accessories) {
	this.accessories = accessories;
}

public String getFanShop() {
	return fanShop;
}

public void setFanShop(String fanShop) {
	this.fanShop = fanShop;
}

public String getFeatured() {
	return featured;
}

public void setFeatured(String featured) {
	this.featured = featured;
}

public String getClearance() {
	return clearance;
}

public void setClearance(String clearance) {
	this.clearance = clearance;
}


	
}
