package com.serve.DicksSportingGoods.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "categories")
public class Categories implements Serializable{
private static final long serialVersionUID = 1L;

@Id
private int categoryId;

@Column(name = "name")
private String name;

@Column(name = "parent_category_id")
private int parentCategoryId;

@Column(name = "department_id")
private int departmentId;

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getCategoryId() {
	return categoryId;
}

public void setCategoryId(int categoryId) {
	this.categoryId = categoryId;
}

public int getParentCategoryId() {
	return parentCategoryId;
}

public void setParentCategoryId(int parentCategoryId) {
	this.parentCategoryId = parentCategoryId;
}

public int getDepartmentId() {
	return departmentId;
}

public void setDepartmentId(int departmentId) {
	this.departmentId = departmentId;
}

}
