package com.serve.DicksSportingGoods.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


//@Entity
@Table(name = "product")
public class Product implements Serializable {
private static final long serialVersionUID = 1L;

@Id
private int productId;

@Column(name = "product_name")
private String productName;

@Column(name = "size")
private int size;

@Column(name = "colour")
private String colour;

public String getProductName() {
	return productName;
}

public void setProductName(String productName) {
	this.productName = productName;
}

public int getSize() {
	return size;
}

public void setSize(int size) {
	this.size = size;
}

public String getColour() {
	return colour;
}

public void setColour(String colour) {
	this.colour = colour;
}


}
