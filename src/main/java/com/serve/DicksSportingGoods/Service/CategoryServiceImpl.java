package com.serve.DicksSportingGoods.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serve.DicksSportingGoods.Dao.CategoryRepository;
import com.serve.DicksSportingGoods.Dto.CategoriesDto;
import com.serve.DicksSportingGoods.Dto.DepartmentDto;
import com.serve.DicksSportingGoods.Model.Categories;
import com.serve.DicksSportingGoods.Model.Department;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository catrepo;
	
	@Override
	public List<CategoriesDto> findAll(){
		
		List<Categories> category = (List<Categories>) catrepo.findAll();

		List<CategoriesDto> CategoriesDtoList = new ArrayList<CategoriesDto>();
		
		if (category != null) {

			for (Categories cate : category) {
				
			CategoriesDto categoriesDto = new CategoriesDto();
			categoriesDto.setCategoryId(cate.getCategoryId());
			categoriesDto.setDepartmentId(cate.getDepartmentId());
			categoriesDto.setName(cate.getName());
			categoriesDto.setParentCategoryId(cate.getParentCategoryId());
			
			CategoriesDtoList.add(categoriesDto);
				
			}
		}

		return CategoriesDtoList;
		
	}


@Override
public String saveCategories(List<CategoriesDto> categories) {
	
	
	
	List<Categories> categ =  new ArrayList<Categories>();
		
		if(categ != null) {
			
			for(CategoriesDto category : categories) {
				
				Categories categoriesdto = new Categories();
				
				
				categoriesdto.setCategoryId(category.getCategoryId());
				categoriesdto.setDepartmentId(category.getDepartmentId());
				categoriesdto.setName(category.getName());
				categoriesdto.setParentCategoryId(category.getParentCategoryId());
				
				categ.add(categoriesdto);
			}
		}
		
		Iterable<Categories> it = categ;
		catrepo.saveAll(it);
		
		return "sucessfull";
		
	
	}
}
