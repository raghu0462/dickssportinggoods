package com.serve.DicksSportingGoods.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serve.DicksSportingGoods.Dao.DepartmentRepository;
import com.serve.DicksSportingGoods.Dto.DepartmentDto;
import com.serve.DicksSportingGoods.Dto.DepartmentMapping;
import com.serve.DicksSportingGoods.Model.Department;

@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository repo;

	@Autowired
	private DepartmentMapping mapping;

	@Override
	public List<DepartmentDto> findAll() {

		List<Department> department = (List<Department>) repo.findAll();

		List<DepartmentDto> departmentDtoList = mapping.convertEntityToDto(department);

		return departmentDtoList;
	}

	@Override
	public String saveDepartments(List<DepartmentDto> department) {

		List<Department> depart = mapping.convertDtoIntoEntity(department);
		
		Iterable<Department> itt = repo.saveAll(depart);
		
		if (itt == null) {
			return "UnSucessfully";
		} else {
			return "Sucessfully Saved";
		}
	}

	// accessModifier ReturnType methodName(parameters, parametrs) {}
}
