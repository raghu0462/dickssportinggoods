package com.serve.DicksSportingGoods.Service;

import java.util.List;

import com.serve.DicksSportingGoods.Dto.CategoriesDto;

public interface CategoryService {

	public List<CategoriesDto> findAll();
	

	String saveCategories(List<CategoriesDto> categories);

}
